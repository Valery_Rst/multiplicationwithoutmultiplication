package net.multiplication;
import java.util.Scanner;

public class MultiplicationWithoutMultiplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите первое значение: ");
        int firstValue = scanner.nextInt();
        System.out.print("Введите второе значение: ");
        int secondValue = scanner.nextInt();

        System.out.printf("Произведенгие выражения %d * %d = %d", firstValue,secondValue, multiplication(firstValue, secondValue));
    }

    private static int multiplication(int firstValue, int secondValue) {
        int result = 0;

        if (firstValue == 0 || secondValue == 0) return 0;

        if (firstValue < 0 && secondValue < 0) {
            firstValue *= -1;
            secondValue *= -1;
        }

        if (secondValue < 0) {
            firstValue *= -1;
            secondValue *= -1;
        }

        for (int i = 1; i <= secondValue; i++){
            result += firstValue;
        }

        return result;
    }
}
